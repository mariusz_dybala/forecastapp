using System.Threading.Tasks;
using UIKit;

namespace NNChallenge.iOS
{
    public static class NavigationServiceExtension
    {
        public static async Task NavigateTo(this UINavigationController navigationController, string parameter,BaseViewController from, BaseViewController to)
        {
            var forecastView = new ForecastViewController();
            
            from.StartLoading();

            await to.DataContext.OnNavigateTo(parameter);
            
            from.EndLoading();
            
            navigationController.PushViewController(forecastView, true);
        }
    }
}