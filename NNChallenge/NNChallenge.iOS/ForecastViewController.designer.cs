﻿// WARNING
//
// This file has been generated automatically by Rider IDE
//   to store outlets and actions made in Xcode.
// If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace NNChallenge.iOS
{
	[Register ("ForecastViewController")]
	partial class ForecastViewController
	{
		[Outlet]
		UIKit.UILabel CityLabel { get; set; }

		[Outlet]
		UIKit.UITableView ForecastTableView { get; set; }

		void ReleaseDesignerOutlets ()
		{
			if (CityLabel != null) {
				CityLabel.Dispose ();
				CityLabel = null;
			}

			if (ForecastTableView != null) {
				ForecastTableView.Dispose ();
				ForecastTableView = null;
			}

		}
	}
}
