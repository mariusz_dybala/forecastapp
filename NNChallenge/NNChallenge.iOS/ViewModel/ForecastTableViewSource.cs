using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using Foundation;
using NNChallenge.Interfaces;
using UIKit;

namespace NNChallenge.iOS.ViewModel
{
    public class ForecastTableViewSource : UITableViewSource
    {
        private readonly IList<HourWeatherForecastVO> _weatherItems;
        private readonly Dictionary<int, NSData> _cachedImages;

        public ForecastTableViewSource(IList<HourWeatherForecastVO> weatherItems)
        {
            _weatherItems = weatherItems;
            _cachedImages = new Dictionary<int, NSData>();
        }

        public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
        {
            var cell = (HourForecastCell) tableView.DequeueReusableCell(HourForecastCell.Key, indexPath);

            var item = _weatherItems[indexPath.Row];
            cell.InitialView(item.TeperatureCelcius, item.TeperatureFahrenheit, item.Date,
                TryGetCachedImage(indexPath.Row, item.ForecastPitureURL));
            return cell;
        }

        private void CacheImage(int key, string imagePath)
        {
            var imageUrl = NSUrl.FromString(imagePath);
            var imageContent = NSData.FromUrl(imageUrl);
            _cachedImages[key] = imageContent;
        }

        private NSData TryGetCachedImage(int key, string imagePath)
        {
            if (!_cachedImages.ContainsKey(key) || _cachedImages[key] == null)
            {
                CacheImage(key, imagePath);
            }

            return _cachedImages[key];
        }

        public override nint RowsInSection(UITableView tableview, nint section)
        {
            return _weatherItems.Count;
        }
    }
}