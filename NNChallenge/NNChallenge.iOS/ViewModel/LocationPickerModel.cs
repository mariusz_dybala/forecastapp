﻿using System;
using UIKit;

namespace NNChallenge.iOS.ViewModel
{
    public class LocationPickerModel : UIPickerViewModel
    {        
        public readonly string[] Locations;

        public LocationPickerModel(string[] locations)
        {
            Locations = locations;
        }

        public override nint GetComponentCount(UIPickerView pickerView)
        {
            return 1;
        }

        public override nint GetRowsInComponent(UIPickerView pickerView, nint component)
        {
            return Locations.Length;
        }

        public override string GetTitle(UIPickerView pickerView, nint row, nint component)
        {
            return Locations[row];
        }

        public override nfloat GetComponentWidth(UIPickerView picker, nint component)
        {
            return 240f;
        }

        public override nfloat GetRowHeight(UIPickerView picker, nint component)
        {
            return 40f;
        }
    }
}
