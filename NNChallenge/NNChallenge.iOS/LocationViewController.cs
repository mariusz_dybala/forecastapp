﻿using System;
using System.Threading.Tasks;
using NNChallenge.Constants;
using NNChallenge.iOS.ViewModel;
using NNChallenge.ViewModels;
using UIKit;

namespace NNChallenge.iOS
{
    public partial class LocationViewController : BaseViewController
    {
        public override INavigatableViewModel DataContext { get; } //No needed in this case. However normally it should be set.
        private LocationPickerModel PickerModel { get; set; }

        public LocationViewController() : base("LocationViewController")
        {
        }       

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
            Title = "Location";
            _submitButton.TitleLabel.Text = "Submit";
            _contentLabel.Text = "Select your location.";
            _submitButton.TouchUpInside += async (s, e) =>
            {                    
                await SubmitButtonTouchUpInside(s, e);
            };
            
            PickerModel = new LocationPickerModel(LocationConstants.LOCATIONS);

            _picker.Model = PickerModel;
        }


        private async Task SubmitButtonTouchUpInside(object sender, EventArgs e)
        {
            var selectedIndex = _picker.SelectedRowInComponent(0);
            var selectedItem = PickerModel.Locations[selectedIndex];
            
            var forecastView = new ForecastViewController();
            await NavigationController.NavigateTo(selectedItem, this,forecastView);
        }

        public override void StartLoading()
        {
            ActivityIndicator.Hidden = false;
        }

        public override void EndLoading()
        {
            ActivityIndicator.Hidden = true;
        }
    }
}

