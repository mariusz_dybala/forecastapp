﻿// WARNING
//
// This file has been generated automatically by Rider IDE
//   to store outlets and actions made in Xcode.
// If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace NNChallenge.iOS
{
	[Register ("HourForecastCell")]
	partial class HourForecastCell
	{
		[Outlet]
		UIKit.UILabel DateTimeLabel { get; set; }

		[Outlet]
		UIKit.UIImageView ForecastImageView { get; set; }

		[Outlet]
		UIKit.UILabel TemperatureLabel { get; set; }

		void ReleaseDesignerOutlets ()
		{
			if (DateTimeLabel != null) {
				DateTimeLabel.Dispose ();
				DateTimeLabel = null;
			}

			if (ForecastImageView != null) {
				ForecastImageView.Dispose ();
				ForecastImageView = null;
			}

			if (TemperatureLabel != null) {
				TemperatureLabel.Dispose ();
				TemperatureLabel = null;
			}

		}
	}
}
