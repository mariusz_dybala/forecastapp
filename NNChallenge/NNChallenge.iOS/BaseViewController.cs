using NNChallenge.ViewModels;
using UIKit;

namespace NNChallenge.iOS
{
    public abstract class BaseViewController : UIViewController
    {
        public abstract INavigatableViewModel DataContext { get; }
        public abstract void StartLoading();
        public abstract void EndLoading();

        public BaseViewController(string name):base(name, null)
        {
        }
    }
}