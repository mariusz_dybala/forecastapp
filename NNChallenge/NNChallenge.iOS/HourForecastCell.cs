﻿using System;
using System.Globalization;
using Foundation;
using UIKit;

namespace NNChallenge.iOS
{
    public partial class HourForecastCell : UITableViewCell
    {
        public static readonly NSString Key = new NSString("HourForecastCell");
        public static readonly UINib Nib;


        static HourForecastCell()
        {
            Nib = UINib.FromName("HourForecastCell", NSBundle.MainBundle);
        }

        protected HourForecastCell(IntPtr handle) : base(handle)
        {
            // Note: this .ctor should not contain any initialization logic.
        }

        public void InitialView(double itemTemperatureC, double itemTemperatureF, DateTime itemDate, NSData cachedImage)
        {
            TemperatureLabel.Text =
                $"{itemTemperatureC.ToString("F", new CultureInfo("en-US"))}C / {itemTemperatureF.ToString("F", new CultureInfo("en-US"))}F";
            DateTimeLabel.Text = itemDate.ToString("g");

            ForecastImageView.Image = UIImage.LoadFromData(cachedImage);
        }
    }
}