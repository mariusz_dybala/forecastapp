﻿using System;
using IoC;
using NNChallenge.Constants;
using NNChallenge.Interfaces;
using NNChallenge.IoC;
using NNChallenge.iOS.ViewModel;
using NNChallenge.ViewModels;
using UIKit;

namespace NNChallenge.iOS
{
    public partial class ForecastViewController : BaseViewController
    {
        public override INavigatableViewModel DataContext =>
            AppContainer.ContainerInstance.Resolve<ForecastViewModel>();
        private ForecastViewModel VM => DataContext as ForecastViewModel;
        
        public ForecastViewController() : base("ForecastViewController")
        {
        }
        
        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
            Title = "Forecast";
            ForecastTableView.RegisterNibForCellReuse(HourForecastCell.Nib, HourForecastCell.Key);
            
            ForecastTableView.RowHeight = UITableView.AutomaticDimension;
            ForecastTableView.EstimatedRowHeight = 100;
            ForecastTableView.SeparatorStyle = UITableViewCellSeparatorStyle.SingleLineEtched;

            CityLabel.Text = VM.City;
            ForecastTableView.Source = new ForecastTableViewSource(VM.WeatherForecastForHours);
        }
        
        public override void StartLoading()
        {
            // it should be implemented bo it's not in the scope of this task
        }

        public override void EndLoading()
        {
            // it should be implemented bo it's not in the scope of this task
        }

        public override void DidReceiveMemoryWarning()
        {
            base.DidReceiveMemoryWarning();
            // Release any cached data, images, etc that aren't in use.
        }
    }
}

