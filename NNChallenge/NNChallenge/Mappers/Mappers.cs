using System;
using NNChallenge.DTOs;
using NNChallenge.Interfaces;

namespace NNChallenge.Mappers
{
    public static class Mappers
    {
        public static HourWeatherForecastVO MapFrom(this HourDto forecastModelDto)
        {
            var hourWeather = new HourWeatherForecastVO
            {
                Date = forecastModelDto.Time,
                TeperatureCelcius = Math.Round(forecastModelDto.TempC, 2, MidpointRounding.AwayFromZero),
                TeperatureFahrenheit = Math.Round(forecastModelDto.TempF, 2, MidpointRounding.AwayFromZero),
                ForecastPitureURL = $"http:{forecastModelDto.Condition.Icon}"
            };

            return hourWeather;
        }
    }
}