using System;
using System.Collections.Generic;
using IoC;
using NNChallenge.Interfaces;
using NNChallenge.Services;
using NNChallenge.ViewModels;

namespace NNChallenge.IoC
{
    public class AppContainer : IConfiguration
    {
        private static IMutableContainer _container;
        public static IMutableContainer ContainerInstance
        {
            get
            {
                if (_container == null)
                {
                    _container = Container.Create().Using<AppContainer>();
                }

                return _container;
            }
        }
        
        public IEnumerable<IToken> Apply(IMutableContainer container)
        {
            yield return container
                .Bind<IWeatherForcastVO>().To<WeatherForecastVO>()
                .Bind<IHttpClientService>().To<HttpClientService>();

            yield return container
                .Bind<ForecastViewModel>().As(Lifetime.Singleton).To<ForecastViewModel>();

        }
    }
}