using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;
using NNChallenge.DTOs;
using NNChallenge.Interfaces;

namespace NNChallenge.Services
{
    public class HttpClientService : IHttpClientService
    {
        private const string BaseUrl =
            "http://api.weatherapi.com/v1/forecast.json?key=898147f83a734b7dbaa95705211612&days=3&aqi=no&alerts=no";

        private static readonly HttpClient HttpClient = new HttpClient()
        {
            BaseAddress = new Uri(BaseUrl),
        };
        
        public async Task<string> GetStringResponseAsync(string parameter)
        {
            using var httpClient = new HttpClient();

            var response = await HttpClient.GetStringAsync(BaseUrl + $"&q={parameter}");

            if (string.IsNullOrWhiteSpace(response))
            {
                return string.Empty;
            }

            return response;
        }
    }
}