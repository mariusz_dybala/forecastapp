using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;
using NNChallenge.DTOs;
using NNChallenge.Interfaces;
using NNChallenge.Mappers;

namespace NNChallenge.Services
{
    public class WeatherForecastVO : IWeatherForcastVO
    {
        private readonly IHttpClientService _httpClientService;

        public WeatherForecastVO(IHttpClientService httpClientService)
        {
            _httpClientService = httpClientService;
        }

        public async Task<IList<HourWeatherForecastVO>> GetHourForecast(string city)
        {
            var response = await _httpClientService.GetStringResponseAsync(city);

            if (string.IsNullOrWhiteSpace(response))
            {
                return new List<HourWeatherForecastVO>();
            }

            var items = JsonConvert.DeserializeObject<ForecastModelDto>(response);

            var itemsToReturn = items.Forecast.ForecastDay
                .SelectMany(x => x.Hour)
                .Select(x => x.MapFrom());

            return itemsToReturn.ToList();
        }
    }
}