using System.Threading.Tasks;

namespace NNChallenge.Interfaces
{
    public interface IHttpClientService
    {
        Task<string> GetStringResponseAsync(string parameter);
    }
}