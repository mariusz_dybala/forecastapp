﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace NNChallenge.Interfaces
{
    public interface IWeatherForcastVO
    {
        Task<IList<HourWeatherForecastVO>> GetHourForecast(string city);
    }

    public class HourWeatherForecastVO
    {
        /// <summary>
        /// date of forecast
        /// </summary>
        public DateTime Date { get; set; }
        /// <summary>
        /// temerature in Celcius
        /// </summary>
        public double TeperatureCelcius { get; set;}
        /// <summary>
        /// Temperture in Fahrenheit
        /// </summary>
        public double TeperatureFahrenheit { get; set;}
        /// <summary>
        /// url for picture
        /// </summary>
        public string ForecastPitureURL { get; set;}
    }
}
