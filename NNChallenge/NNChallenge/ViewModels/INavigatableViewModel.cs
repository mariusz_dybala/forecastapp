using System.Threading.Tasks;

namespace NNChallenge.ViewModels
{
    public interface INavigatableViewModel
    {
        Task OnNavigateTo(string parameter);
    }
}