using System.Collections.Generic;
using System.Threading.Tasks;
using NNChallenge.Interfaces;

namespace NNChallenge.ViewModels
{
    public class ForecastViewModel : INavigatableViewModel
    {
        public readonly IWeatherForcastVO _weatherForcastVo;
        
        public string City { get; set; }
        public IList<HourWeatherForecastVO> WeatherForecastForHours { get; set; }

        public ForecastViewModel(IWeatherForcastVO weatherForcastVo)
        {
            _weatherForcastVo = weatherForcastVo;
        }

        public async Task OnNavigateTo(string parameter)
        {
            WeatherForecastForHours = await _weatherForcastVo.GetHourForecast(parameter);
            City = parameter;
        }
    }
}