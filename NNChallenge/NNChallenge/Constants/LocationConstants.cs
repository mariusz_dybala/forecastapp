﻿namespace NNChallenge.Constants
{
    public static class LocationConstants
    {
        public static readonly string Berlin  = "Berlin";
        public static readonly string Moscow = "Moscow";
        public static readonly string NewYork = "New York";
        public static readonly string Tokyo = "Tokyo";
        
        public static readonly string[] LOCATIONS = new string[] { Berlin, Moscow, NewYork, Tokyo};
    }
}
