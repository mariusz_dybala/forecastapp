using System;
using System.Collections.Generic;

namespace NNChallenge.DTOs
{
    public class ForecastDto
    {
        public IList<HourDto> Hour { get; set; }
    }
}