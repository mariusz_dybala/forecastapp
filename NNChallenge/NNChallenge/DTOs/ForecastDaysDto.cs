using System.Collections.Generic;

namespace NNChallenge.DTOs
{
    public class ForecastDaysDto
    {
        public IList<ForecastDto> ForecastDay { get; set; }
    }
}