using System;
using Newtonsoft.Json;

namespace NNChallenge.DTOs
{
    public class HourDto
    {
        [JsonProperty("temp_c")]
        public float TempC { get; set; }
        [JsonProperty("temp_f")]
        public float TempF { get; set; }
        public DateTime Time { get; set; }
        public ConditionDto Condition { get; set; }
    }
}