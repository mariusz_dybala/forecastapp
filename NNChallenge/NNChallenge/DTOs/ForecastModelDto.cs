namespace NNChallenge.DTOs
{
    public class ForecastModelDto
    {
        public LocatioDto Location { get; set; }
        public ForecastDaysDto Forecast { get; set; }
    }
}